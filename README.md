# Paul Theme Typecho

设计原型 <https://paul.ren>

Demo : <https://innei.ren>

**文档请认真仔细的看~**

如果你喜欢，请我一个 Star, 谢谢～

开发不易，让我们向所有开发者致敬！

**此主题经过长期更新与修复，现已可正常使用。**

## 浏览器兼容性

至少需要支持 ES6 的现代浏览器, 推荐使用 Chrome

## ⚠️ 注意

1. PHP > 7.1
1. 评论使用了 Ajax 提交，测试发现发送评论和回复均无异常，但是监视评论的插件如**评论邮件提醒**将会失效。可能是钩子函数的问题，如有解决方式请 PR。谢谢
1. 使用 AppNode 或者 其他面板 的小伙伴请注意，请把网站的PHP设置 `allow_url_fopen = On`

## 特点和实现

- [x] 日记
- [x] 语录
- [x] 主页
- [x] 文章页
- [x] 作品页
- [x] 评论与回复，不一样的输入框
- [x] 点赞, 浏览量
- [x] 播放器
- [x] 音乐页
- [x] ajax 加载更多文章
- [x] 全站无刷新体验 (可能只有 Chrome 支持)
- [x] 评论 ajax 提交
- [x] 文章内图片懒加载 (Safari 未通过测试)
- [x] ajax 登陆后台
- [x] ajax 前台提交新文章，带来不一样的体验
- [x] GitHub 开源页

## 预览

主页:
![image](https://user-images.githubusercontent.com/41265413/59972764-ab828900-95c7-11e9-9d4c-2db223d42e37.png)
![image](https://user-images.githubusercontent.com/41265413/59972769-c3f2a380-95c7-11e9-8f52-534a118757c8.png)

主页模板:
![image](https://user-images.githubusercontent.com/41265413/59972774-cb19b180-95c7-11e9-82c4-bb157e6d6e76.png)

日记页:
![image](https://user-images.githubusercontent.com/41265413/59972779-d66cdd00-95c7-11e9-8f9d-25b1804d662c.png)
![image](https://user-images.githubusercontent.com/41265413/59972796-2186f000-95c8-11e9-857d-5ed2b2ceb86d.png)

日记详细页:
![image](https://user-images.githubusercontent.com/41265413/59972782-e8e71680-95c7-11e9-91f6-4a75828f80fe.png)
![image](https://user-images.githubusercontent.com/41265413/59972798-2ea3df00-95c8-11e9-8ebd-1f70af2dd878.png)

作品页:
![image](https://user-images.githubusercontent.com/41265413/59972787-fbf9e680-95c7-11e9-8e12-878ae9c11cc0.png)

作品信息页:
![image](https://user-images.githubusercontent.com/41265413/59972786-f6040580-95c7-11e9-8713-f27dfc466fd9.png)

追番页:
![image](https://user-images.githubusercontent.com/41265413/59441120-a9cbff00-8e2a-11e9-87ef-241ff0edccdc.png)

归档页：
![image](https://user-images.githubusercontent.com/41265413/59972789-0a480280-95c8-11e9-86b5-1f7fd1f89e7e.png)

歌单：
![image](https://user-images.githubusercontent.com/41265413/59972793-146a0100-95c8-11e9-80f6-9ec672cc0351.png)

GitHub 开源页:
![](https://raw.githubusercontent.com/Innei/img-bed/master/20190627131326.png)

## 故事

前往 [Pual Typecho主题发布](https://shizuri.net/archives/131/).

## 开始之前

这是一款适合写日记，也适合用于个人主页展示的主题。

他的原出处来源于 <https://paul.ren> ，此移植主题现在仍有不完善的地方，一是因为 Typecho 的限制，二是因为时间比较匆忙。已实现的功能见上。

## 快速开始

Clone 此项目，在 设置 中使用此主题。在设置主题中填写相关字段。

## 使用方法

参见[Typecho主题Paul一些说明以及问答](https://blog.shizuri.net/Z-Turn/Typecho%E4%B8%BB%E9%A2%98Paul%E4%B8%80%E4%BA%9B%E8%AF%B4%E6%98%8E%E4%BB%A5%E5%8F%8A%E9%97%AE%E7%AD%94.html)

## 版权 & 开源

@Dreamer-Paul & @Innei 所有, 开源遵循 MIT.

### 使用的开源项目

- fontawesome 4
- Kico Style
- Kico Player

## 鸣谢

- [@Dreamer-Paul](https://github.com/Dreamer-Paul)
- [@moesoha](https://github.com/moesoha)


